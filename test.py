from datetime import datetime
from time import time

index = datetime.now().strftime("%Y-%m-%d %H:%M:%S %z")
print(index)
       

server {
   listen 80 default_server;
   root /home/pi/Documents/greenhouse-project/web/build;
   server_name your.domain.com;
   index index.html index.htm;
   location / {
   }
}