import pymysql
from datetime import datetime

def data_update_sensors_0(data):
    db = pymysql.connect("localhost","pi_user","123456789","pi_db" )
    cursor = db.cursor() # prepare a cursor object using cursor() method
    # Prepare SQL query to UPDATE required records
    time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    sql = "update sensors_now set time = '%s', temperature = '%s', humidity = '%s', light = '%s' where id = '%s'" %(time, data[0], data[1], data[2], '0')
    try:
        cursor.execute(sql)  # Execute the SQL command
        db.commit() # Commit your changes in the database
    except:
        db.rollback()  # Rollback in case there is any error

    # SELECT test
    #sql = "SELECT * FROM sensors_now "
    #try:
    #    cursor.execute(sql) # Execute the SQL command
    #    results = cursor.fetchall() # Fetch all the rows in a list of lists.
    #   for row in results:
    #       print (row)  # Now print fetched result
    #except:
    #    print ("Error: unable to fetch data")

    db.close()  # disconnect from server


def data_update_sensors_1(data):
    db = pymysql.connect("localhost","pi_user","123456789","pi_db" )
    cursor = db.cursor() # prepare a cursor object using cursor() method
    # Prepare SQL query to UPDATE required records
    time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    sql = "update sensors_now set time = '%s', temperature = '%s', humidity = '%s', light = '%s', water_flow = '%s' where id = '%s'" %(time, data[0], data[1], data[2], data[3], '1')
    try:
        cursor.execute(sql)  # Execute the SQL command
        db.commit() # Commit your changes in the database
    except:
        db.rollback()  # Rollback in case there is any error

    # SELECT test
    # sql = "SELECT * FROM sensors_now "
    # try:
    #     cursor.execute(sql) # Execute the SQL command
    #     results = cursor.fetchall() # Fetch all the rows in a list of lists.
    #     for row in results:
    #         print (row)  # Now print fetched result
    # except:
    #     print ("Error: unable to fetch data")

    db.close()  # disconnect from server
