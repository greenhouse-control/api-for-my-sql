from flask import jsonify
from flask import flash, request
from config import app, mysql, mqtt
import pymysql
import datetime
def flask_app():
    @app.route('/add', methods=['POST'])
    def add_data():
        try:
            _json = request.json
            _id = _json['id']
            _time = _json['time']
            _temperature = _json['temperature']
            _humidity = _json['humidity']
            _lux = _json['lux']
            # validate the received values
            if _id and _time and _temperature and _humidity and _lux:
                # save edits
                sql = "INSERT INTO sensors_now(id, time, temperature, humidity, lux) VALUES(%s, %s, %s, %s, %s)"
                data = (_id, _time, _temperature, _humidity, _lux)
                conn = mysql.connect()
                cursor = conn.cursor()
                cursor.execute(sql, data)
                conn.commit()
                resp = jsonify('User added successfully!')
                resp.status_code = 200
                return resp
            else:
                return not_found()
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conn.close()

    @app.route('/')
    def welcome():
        return('Welcome')
       

    @app.route('/api/sensors-now')
    def sensors_now():
        try:
            conn = mysql.connect()
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            cursor.execute("SELECT * FROM sensors_now")
            rows = cursor.fetchall()
            resp = jsonify(rows)
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conn.close()
    
    @app.route('/api/sensors-statistic-0')
    def sensors_statistic_0():
        try:
            conn = mysql.connect()
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            cursor.execute("SELECT * FROM sensors_statistic_0 ORDER BY time")
            rows = cursor.fetchall()
            resp = jsonify(rows)
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conn.close()
    @app.route('/api/sensors-statistic-0/<string:key>:"<string:field>"<string:operator>"<string:value>"')
    def sensors_statistic__filter_value_0(key, field, operator, value):
        if (key == 'filter'):
            try:
                conn = mysql.connect()
                cursor = conn.cursor(pymysql.cursors.DictCursor)
                cursor.execute("SELECT * FROM sensors_statistic_0 WHERE " +  field + operator + value +"ORDER BY time")
                rows = cursor.fetchall()
                resp = jsonify(rows)
                resp.status_code = 200
                return resp
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                conn.close()
    @app.route('/api/sensors-statistic-0/<string:key>:"<string:field>"from"<string:start>"to"<string:end>"')
    def sensors_statistic__filter_range_0(key, field, start, end):
        if (key == 'filter'):
            try:
                conn = mysql.connect()
                cursor = conn.cursor(pymysql.cursors.DictCursor)
                cursor.execute("SELECT * FROM sensors_statistic_0 WHERE " +  field + " BETWEEN " + start + " AND " + end +"ORDER BY time")
                rows = cursor.fetchall()
                resp = jsonify(rows)
                resp.status_code = 200
                return resp
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                conn.close()

    @app.route('/api/sensors-statistic-1')
    def sensors_statistic_1():
        try:
            conn = mysql.connect()
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            cursor.execute("SELECT * FROM sensors_statistic_1 ORDER BY time")
            rows = cursor.fetchall()
            resp = jsonify(rows)
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conn.close()
    @app.route('/api/sensors-statistic-1/<string:key>:"<string:field>"<string:operator>"<string:value>"')
    def sensors_statistic__filter_value_1(key, field, operator, value):
        if (key == 'filter'):
            try:
                conn = mysql.connect()
                cursor = conn.cursor(pymysql.cursors.DictCursor)
                cursor.execute("SELECT * FROM sensors_statistic_1 WHERE " +  field + operator + value +"ORDER BY time")
                rows = cursor.fetchall()
                resp = jsonify(rows)
                resp.status_code = 200
                return resp
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                conn.close()
    @app.route('/api/sensors-statistic-1/<string:key>:"<string:field>"from"<string:start>"to"<string:end>"')
    def sensors_statistic__filter_range_1(key, field, start, end):
        if (key == 'filter'):
            try:
                conn = mysql.connect()
                cursor = conn.cursor(pymysql.cursors.DictCursor)
                cursor.execute("SELECT * FROM sensors_statistic_1 WHERE " +  field + " BETWEEN " + start + " AND " + end +"ORDER BY time")
                rows = cursor.fetchall()
                resp = jsonify(rows)
                resp.status_code = 200
                return resp
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                conn.close()

    @app.route('/api/switch-control')
    def switch_control():
        try:
            conn = mysql.connect()
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            cursor.execute("SELECT * FROM switch_control")
            rows = cursor.fetchall()
            resp = jsonify(rows)
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conn.close()
    @app.route('/api/switch-control/id:<string:id>')
    def switch_control_select_id(id):
        try:
            conn = mysql.connect()
            cursor = conn.cursor(pymysql.cursors.DictCursor)
            cursor.execute("SELECT * FROM switch_control WHERE id=%s", id)
            rows = cursor.fetchall()
            resp = jsonify(rows)
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conn.close()


    @app.route('/api/update', methods=['PUT'])
    def update_data():
        try:
            _json = request.json
            _id = _json['id']
            _time = _json['timel']
            _temperature = _json['temperature']
            _humidity = _json['humidity']
            _lux = _json['lux']
            # validate the received values
            if _id and _time and _temperature and _humidity and _lux and request.method == 'POST':

                # save edits
                sql = "UPDATE sensors_now SET id=%s time=%s, temperature=%s, humidity=%s lux=%s WHERE id=%s"
                data = (_time, _temperature, _humidity, _lux, _id,)
                conn = mysql.connect()
                cursor = conn.cursor()
                cursor.execute(sql, data)
                conn.commit()
                resp = jsonify('User updated successfully!')
                resp.status_code = 200
                return resp
            else:
                return not_found()
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conn.close()

    @app.route('/api/delete/<int:id>')
    def delete_data(id):
        try:
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("DELETE FROM sensers_now WHERE id=%s", (id))
            conn.commit()
            resp = jsonify('User deleted successfully!')
            resp.status_code = 200
            return resp
        except Exception as e:
            print(e)
        finally:
            cursor.close()
            conn.close()
            
    @app.route('/control/switch-0/<string:switch_id>/<string:status>', methods=['POST'])
    def switch_relay(switch_id, status):
        if (switch_id[0:5] == 'relay') & ((status == 'on') | (status == 'off')):
            print ( switch_id + ' ' + status)
            if (int(switch_id[6:7]) < 4):
                mqtt.publish(topic="control/switch-0/ras-to-ard", payload="turn " + switch_id + " "  + status, qos=2, retain=False)
            elif (int(switch_id[6:7]) < 7):
                mqtt.publish(topic="control/switch-1/ras-to-ard", payload="turn " + switch_id + " "  + status, qos=2, retain=False)
        return ('',204)


    @app.errorhandler(404)
    def not_found(error=None):
        message = {
            'status': 404,
            'message': 'Not Found: ' + request.url,
        }
        resp = jsonify(message)
        resp.status_code = 404

        return resp
