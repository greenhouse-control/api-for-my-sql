import pymysql
from datetime import datetime

def data_insert_sensors_statistic_0(data):
    index = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    db = pymysql.connect("localhost","pi_user","123456789","pi_db" )
    cursor = db.cursor() # prepare a cursor object using cursor() method
    # Prepare SQL query to UPDATE required records
    sql = "INSERT INTO sensors_statistic_0 (id, temperature, humidity, light) VALUES (%s, %s, %s, %s)"
    val = (index, data[0], data[1], data[2])
    try:
        cursor.execute(sql,val)  # Execute the SQL command
        db.commit() # Commit your changes in the database
    except:
        db.rollback()  # Rollback in case there is any error

    # SELECT test
    # sql = "SELECT * FROM sensors_statistic_0 "
    # try:
    #     cursor.execute(sql) # Execute the SQL command
    #     results = cursor.fetchall() # Fetch all the rows in a list of lists.
    #     for row in results:
    #         print (row)  # Now print fetched result
    # except:
    #     print ("Error: unable to fetch data")

    # db.close()  # disconnect from server


def data_insert_sensors_statistic_1(data):
    index = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    db = pymysql.connect("localhost","pi_user","123456789","pi_db" )
    cursor = db.cursor() # prepare a cursor object using cursor() method
    # Prepare SQL query to UPDATE required recordsSerial.println("Disconnected...");
    sql = "INSERT INTO sensors_statistic_1 (id, temperature, humidity, light, water_flow) VALUES (%s, %s, %s, %s, %s)"
    val = (index, data[0], data[1], data[2], data[3])
    try:
        cursor.execute(sql, val)  # Execute the SQL command
        db.commit() # Commit your changes in the database
    except:
        db.rollback()  # Rollback in case there is any error

    # SELECT test
    # sql = "SELECT * FROM sensors_now "
    # try:
    #     cursor.execute(sql) # Execute the SQL command
    #     results = cursor.fetchall() # Fetch all the rows in a list of lists.
    #     for row in results:
    #         print (row)  # Now print fetched result
    # except:
    #     print ("Error: unable to fetch data")

    db.close()  # disconnect from server