import pymysql

def data_update_switch(data):
    db = pymysql.connect("localhost","pi_user","123456789","pi_db" )
    cursor = db.cursor() # prepare a cursor object using cursor() method
    # Prepare SQL query to UPDATE required records
    sql = "UPDATE switch_control SET  name = '%s', status = '%s' WHERE id = '%s'" %( data[2], data[3], data[1])
    try:
        cursor.execute(sql)  # Execute the SQL command
        db.commit() # Commit your changes in the database
    except:
        db.rollback()  # Rollback in case there is any error

    # # SELECT test
    # sql = "SELECT * FROM sensors_now "
    # try:
    #     cursor.execute(sql) # Execute the SQL command
    #     results = cursor.fetchall() # Fetch all the rows in a list of lists.
    #     for row in results:
    #         print (row)  # Now print fetched result
    # except:
    #     print ("Error: unable to fetch data")

    db.close()  # disconnect from server